# --- !Ups

create table app_user (
  id                        bigserial not null,
  email                     varchar(255) not null,
  passwordHash              varchar(255) not null,
  fullName                  varchar(255) not null,
  constraint pk_app_user primary key (id))
;

create table todo_item (
  id                        bigserial not null,
  text                      varchar(255) not null,
  reminder                  timestamp,
  done                      boolean,
  list_id                   bigint not null,
  constraint pk_todo_item primary key (id))
;

create table todo_list (
  id                        bigserial not null,
  name                      varchar(255) not null,
  owner_id                  bigint not null,
  constraint pk_todo_list primary key (id))
;

alter table todo_item add constraint fk_todo_item_list_1 foreign key (list_id) references todo_list (id) on delete restrict on update restrict;
create index ix_todo_item_list_1 on todo_item (list_id);
alter table todo_list add constraint fk_todo_list_owner_2 foreign key (owner_id) references app_user (id) on delete restrict on update restrict;
create index ix_todo_list_owner_2 on todo_list (owner_id);

insert into app_user(email, passwordHash, fullName)
  values ('nclement@yahoo.com.au', '$2a$10$Di9zwE27KpwePQcKnMbfwecf2IlC6hPtelwrNMBDmP5RZpm2ZTmVi', 'Nathan Clement');
insert into todo_list(owner_id, name)
  values (1, 'Groceries');
insert into todo_list(owner_id, name)
  values (1, 'Work');



# --- !Downs

drop table if exists todo_item;

drop table if exists todo_list;

drop table if exists app_user;

