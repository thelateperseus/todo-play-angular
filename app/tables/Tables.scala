package tables;

import java.sql.Timestamp
import java.util.Date

import models.AppUser
import models.TodoItem
import models.TodoList
import slick.driver.JdbcProfile
import slick.lifted.ProvenShape.proveShapeOf

trait Tables {
  protected val driver = slick.driver.PostgresDriver
  import driver.api._

  implicit def dateTime  =
    MappedColumnType.base[Date, Timestamp](
      dt => new Timestamp(dt.getTime),
      ts => new Date(ts.getTime)
  )

  class AppUsers(tag: Tag) extends Table[AppUser](tag, "app_user") {
  
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def email = column[String]("email")
    def passwordHash = column[String]("passwordhash")
    def fullName = column[String]("fullname")

    def * = (id.?, email, passwordHash, fullName) <> ((AppUser.apply _).tupled, AppUser.unapply _)
  }

  class TodoLists(tag: Tag) extends Table[TodoList](tag, "todo_list") {
  
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def ownerId = column[Long]("owner_id")
    def name = column[String]("name")

    def * = (id.?, ownerId.?, name) <> ((TodoList.apply _).tupled, TodoList.unapply _)
  }

  class TodoItems(tag: Tag) extends Table[TodoItem](tag, "todo_item") {
  
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def listId = column[Long]("list_id")
    def text = column[String]("text")
    def reminder = column[Option[Date]]("reminder")
    def done = column[Boolean]("done")
    def list = foreignKey("todo_list", listId, TableQuery[TodoLists])(_.id)

    def * = (id.?, listId, text, reminder, done) <> ((TodoItem.apply _).tupled, TodoItem.unapply _)
  }
}