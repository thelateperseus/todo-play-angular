package controllers

import play.api._
import play.api.mvc._
import play.api.http.MimeTypes
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.Json.toJson
import tables.Tables
import play.api.Play
import slick.driver.JdbcProfile
import models.AppUser
import play.api.libs.json.JsObject
import scala.concurrent.Future
import org.mindrot.jbcrypt.BCrypt;
import slick.backend.DatabaseConfig

class Application extends Controller with Tables {

  import driver.api._
  val db = Database.forName("DefaultDS")

  //create an instance of the table
  val appUsers = TableQuery[AppUsers]

  def index = Action {
    Ok(views.html.index())
  }

  def login() = Action.async(parse.json) { implicit request =>
    val login = request.body.as[JsObject]
    val email = login.\("email").get.as[String]
    val password = login.\("password").get.as[String]

    db.run(appUsers.filter(_.email === email).result.headOption).map { appUser =>
      appUser match {
        case Some(user) => {
          if (BCrypt.checkpw(password, user.passwordHash))
            Ok(toJson(appUser)).withSession(Security.username -> user.id.get.toString)
          else
            Unauthorized
        }
        case None => NotFound
      }
    }
  }

  def logout() = Action { implicit request =>
    Ok("{}").withNewSession
  }

  def profile = Action.async { implicit request =>
    request.session.get(Security.username) match {
      case Some(userId) => {
        db.run(appUsers.filter(_.id === userId.toLong).result.headOption).map { appUser =>
          appUser match {
            case Some(appUser) => Ok(toJson(appUser))
            case None => NoContent
          }
        }
      }
      case None => Future.successful(NoContent)
    }
  }

}