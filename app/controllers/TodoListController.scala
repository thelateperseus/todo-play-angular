package controllers

import scala.concurrent.Future
import play.api._
import play.api.mvc._
import javax.inject.Inject
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.Json.toJson
import slick.driver.JdbcProfile
import models.TodoList
import play.api.libs.json.JsSuccess
import play.api.libs.json.JsError
import tables.Tables
import play.api.libs.json.JsObject

class TodoListController extends Controller with Tables {

  import driver.api._
  val db = Database.forName("DefaultDS")
 
  // TODO remove hardcoding of user to 1
  def listById(id: Long, userId: Long) = { 
    list: TodoLists => list.id === id && list.ownerId === userId 
  }

  //create an instance of the table
  val todoLists = TableQuery[TodoLists]
  val todoItems = TableQuery[TodoItems]


  def viewLists() = AuthenticatedAction.async { implicit request =>
    val userId = request.user.toLong;
    db.run(todoLists.filter(_.ownerId === userId).sortBy(_.name).result).map { lists => 
      Ok(toJson(lists)) 
    }
  }

  def addList() = AuthenticatedAction.async(parse.json) { implicit request =>
    val userId = request.user.toLong;
    request.body.validate[TodoList] match {
      case s: JsSuccess[TodoList] => {
        val list = s.get.copy(ownerId=Some(userId))
        db.run(
          // This is the horrible way we have to get the generated id in a new model
          (todoLists returning todoLists.map(_.id)
                     into ((list, id) => list.copy(id=Some(id)))
          ) += list).map(newList => Ok(toJson(newList)))
      }
      case e: JsError => {
        Future.successful(UnprocessableEntity(JsError.toJson(e)))
      }
    }
  }

  def viewList(id: Long) = AuthenticatedAction.async { implicit request =>
    val userId = request.user.toLong
    for {
      list <- db.run(todoLists.filter(listById(id, userId)).result.headOption)
      items <- db.run(todoItems.filter(_.listId === id).sortBy(_.text).result)
    } yield list match {
      case Some(_) => Ok(toJson(list).as[JsObject] + ("items" -> toJson(items)))
      case None => NotFound
    }
  }

  def updateList(id: Long) = AuthenticatedAction.async(parse.json) { implicit request =>
    val userId = request.user.toLong;
    request.body.validate[TodoList] match {
      case s: JsSuccess[TodoList] => {
        val updatedList: TodoList = s.get.copy(id=Some(id), ownerId=Some(userId))
        db.run(todoLists.filter(listById(id, userId)).update(updatedList)).map( updateCount =>
          updateCount match {
            /* Would like to return the model without the items Ok(toJson(updatedList)), but that 
             * messes up the Angular resource on the client side because the items are missing. */
            case 1 => NoContent
            case default => NotFound
          })
      }
      case e: JsError => {
        Future.successful(UnprocessableEntity(JsError.toJson(e)))
      }
    }
  }

}
