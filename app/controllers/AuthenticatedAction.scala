package controllers

import play.api.mvc.Security
import play.api.mvc.Security.AuthenticatedBuilder

object AuthenticatedAction extends AuthenticatedBuilder(req => req.session.get(Security.username))
