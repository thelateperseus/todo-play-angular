package controllers

import scala.concurrent.Future
import play.api._
import play.api.mvc._
import javax.inject.Inject
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.Json.toJson
import slick.driver.JdbcProfile
import models.TodoList
import play.api.libs.json.JsSuccess
import play.api.libs.json.JsError
import tables.Tables
import play.api.libs.json.JsObject
import models.TodoItem
import play.api.libs.json.JsDefined
import play.api.mvc.Security.AuthenticatedBuilder

class TodoItemController extends Controller with Tables {

  import driver.api._
  val db = Database.forName("DefaultDS")
  
  def listById(id: Long, userId: Long) = { 
    list: TodoLists => list.id === id && list.ownerId === userId 
  }

  //create an instance of the table
  val todoLists = TableQuery[TodoLists]
  val todoItems = TableQuery[TodoItems]

  def addItem() = AuthenticatedAction.async(parse.json) { implicit request =>
    val userId = request.user.toLong;
    request.body.validate[TodoItem] match {
      case s: JsSuccess[TodoItem] => {
        val item = s.get.copy(id=None)
        db.run(todoLists.filter(listById(item.listId, userId)).result.headOption).flatMap { list =>
          list match {
            case Some(_) => {
              db.run(
                // This is the horrible way we have to get the generated id in a new model
                (todoItems returning todoItems.map(_.id)
                           into ((item, id) => item.copy(id=Some(id)))
                ) += item).map(newItem => Ok(toJson(newItem)))
            }
            case None => Future.successful(BadRequest)
          }
        }
      }
      case e: JsError => {
        Future.successful(UnprocessableEntity(JsError.toJson(e)))
      }
    }
  }

  def editItem(id: Long) = AuthenticatedAction.async(parse.json) { implicit request =>
    val userId = request.user.toLong;
    request.body.validate[TodoItem] match {
      case s: JsSuccess[TodoItem] => {
        val updatedItem = s.get.copy(id=Some(id))
        processItem(id, userId, { item => {
          db.run(todoItems.filter(_.id === id).update(updatedItem)).map { updateCount =>
            updateCount match {
              case 1 => Ok(toJson(updatedItem))
              case default => NotFound
            }
          }
        }})
      }
      case e: JsError => {
        Future.successful(UnprocessableEntity(JsError.toJson(e)))
      }
    }
  }

  def updateDone(id: Long) = AuthenticatedAction.async(parse.json) { implicit request =>
    val userId = request.user.toLong;
    processItem(id, userId, { item =>
      val done = request.body.as[JsObject].\("done").get.as[Boolean]
      val updatedItem = item.copy(done=done)
      db.run(todoItems.filter(_.id === id).update(updatedItem)).map { updateCount =>
        updateCount match {
          case 1 => Ok(toJson(updatedItem))
          case default => NotFound
        }
      }
    })
  }

  def deleteItem(id: Long) = AuthenticatedAction.async { implicit request =>
    val userId = request.user.toLong;
    processItem(id, userId, { item => {
      db.run(todoItems.filter(_.id === id).delete).map(_ => Ok)
    }})
  }

  private def processItem(id: Long, userId: Long, f: TodoItem => Future[Result]): Future[Result] = {
    // Ensure we're working with an item in a list owned by the user
    val itemQuery = todoItems.join(todoLists).on(_.listId === _.id)
      .filter { case (item, list) => item.id === id && list.ownerId === userId }
    // If the above query has a result, process the item
    db.run(itemQuery.result.headOption).flatMap { result =>
      result match {
        case Some((item, list)) => f(item)
        case None => Future.successful(NotFound)
      }
    }
  }

}
