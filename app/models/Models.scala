package models

import java.util.Date
import play.api.libs.json._
import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._

case class AppUser(id: Option[Long], email: String, passwordHash: String, fullName: String)

object AppUser {
  implicit val appUserWrites: Writes[AppUser] = (
    (JsPath \ "id").writeNullable[Long] and
    (JsPath \ "email").write[String] and
    (JsPath \ "fullName").write[String]
  )((u: AppUser) => (u.id, u.email, u.fullName))
}

case class TodoList(id: Option[Long], ownerId: Option[Long], name: String)

object TodoList {
  implicit val todoListReads: Reads[TodoList] = (
    (JsPath \ "id").readNullable[Long] and
    (JsPath \ "ownerId").readNullable[Long] and
    (JsPath \ "name").read[String](minLength[String](1))
  )(TodoList.apply _)

  implicit val todoListWrites = Json.writes[TodoList]
}

case class TodoItem(id: Option[Long], listId: Long, text: String, reminder: Option[Date], done: Boolean)

object TodoItem {
  implicit val todoItemReads: Reads[TodoItem] = (
    (JsPath \ "id").readNullable[Long] and
    (JsPath \ "listId").read[Long] and
    (JsPath \ "text").read[String](minLength[String](1)) and
    (JsPath \ "reminder").readNullable[Date] and
    (JsPath \ "done").read[Boolean]
  )(TodoItem.apply _)

  implicit val todoItemWrites = Json.writes[TodoItem]
}
