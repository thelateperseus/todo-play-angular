/*global define */

'use strict';

define(['angular'], function(angular) {

  var todoControllers = angular.module('todoControllers', []);

  todoControllers.controller('HomeCtrl', ['$scope',
    function($scope) {
    }]);
  
  todoControllers.controller('ApplicationCtrl', ['$scope', '$state', '$http',
    function ($scope, $state, $http) {
      $scope.currentUser = null;

      // Check if the user is logged in when the app is loaded
      $http.get('api/v1/profile')
        .then(function(user) {
          if (user.data) {
            $scope.setCurrentUser(user.data);
          }
        });

      $scope.setCurrentUser = function (user) {
        $scope.currentUser = user;
      };
      
      $scope.logout = function() {
        $scope.setCurrentUser(null);
        $http.get('api/v1/logout');
        $state.go('home');
      }
    }]);

  todoControllers.controller('LoginCtrl', ['$scope', '$http', 'authService',
    function ($scope, $http, authService) {
      $scope.loginDisplayed = false;
      $scope.credentials = {
        email: '',
        password: ''
      };

      $scope.login = function (credentials) {
        $scope.errorMessage = null;
        $http({ignoreAuthModule: true, method: 'POST', url: 'api/v1/login', data: credentials})
          .then(function(user) {
            authService.loginConfirmed(user);
            $scope.setCurrentUser(user);
          }, function(error) {
            console.log(error);
            $scope.errorMessage = "Invalid email or password";
          });
      };
    }]);

  todoControllers.controller('TodoListsCtrl', ['$scope', 'TodoList',
    function($scope, TodoList) {
      $scope.lists = TodoList.query();
    }]);

  todoControllers.controller('NewListCtrl', ['$scope', '$state', 'TodoList',
    function($scope, $state, TodoList) {
      $scope.name = "";
      document.getElementById('name').focus();

      $scope.save = function() {
        var newList = new TodoList({name: $scope.name});
        $scope.hasErrors = false;

        newList.$save(function(data) {
          // Add new list to left menu
          $scope.$parent.lists.push(data);
        }, function(error) {
          $scope.errors = error.data;
          $scope.hasErrors = true;
        });
      };
    }]);

  todoControllers.controller('ListDetailCtrl', ['$scope', '$state', '$stateParams', 'TodoList', 'TodoItem',
    function($scope, $state, $stateParams, TodoList, TodoItem) {
      $scope.list = TodoList.get({listId: $stateParams.listId});
      $scope.isAddingItem = false;
      $scope.datepickerOpened = false;
      
      $scope.openDatepicker = function($event) {
        $scope.datepickerOpened = true;
      };

      $scope.edit = function() {
        $scope.isEditing = true;
      };

      $scope.addItem = function() {
        $scope.newText = null;
        $scope.isAddingItem = true;
      };

      $scope.saveNewItem = function() {
        var newItem = new TodoItem({
          listId: $scope.list.id, text: $scope.newText, reminder: $scope.newReminder, done: false});
        $scope.hasErrors = false;

        newItem.$save(function(data) {
          $scope.list.items.push(data);
          $scope.isAddingItem = false;
        }, function(error) {
          $scope.errors = error.data;
          $scope.hasErrors = true;
        });
      };

      $scope.cancelAddItem = function() {
        $scope.isAddingItem = false;
      };

      $scope.acceptChanges = function() {
        if ($scope.isEditing) {
          $scope.hasErrors = false;
          $scope.list.$update({listId: $stateParams.listId}, function(data) {
            // Update existing list in model
            $($scope.$parent.lists).each(function(index, list) {
              if (list.id === Number($stateParams.listId)) {
                list.name = $scope.list.name;
              }
            });
            $scope.isEditing = false;
          }, function(error) {
              $scope.errors = error.data;
              $scope.hasErrors = true;
            });
        }
      };
    }]);

  todoControllers.controller('ItemCtrl', ['$scope', '$state', '$modal', '$http', 'TodoItem',
    function($scope, $state, $modal, $http, TodoItem) {
      $scope.isEditing = false;
      $scope.datepickerOpened = false;
      
      $scope.openDatepicker = function($event) {
        $scope.datepickerOpened = true;
      };

      $scope.editItem = function() {
        $scope.isEditing = true;
      };

      $scope.cancelEditing = function() {
        // TODO Revert edits somehow?
        $scope.isEditing = false;
      };

      $scope.saveItem = function() {
        $scope.hasErrors = false;

        new TodoItem($scope.item).$update({itemId: $scope.item.id}, function(data) {
          $scope.isEditing = false;
          // Model is already updated
        }, function(error) {
          $scope.errors = error.data;
          $scope.hasErrors = true;
        });
      };

      Object.defineProperty($scope, 'isDone', {
        get: function() {
          return $scope.item.done;
        },
        set: function(val) {
          if ( $scope.item.done !== val ) {
            $http.patch('/api/v1/todoItems/' + $scope.item.id, {done: val})
              .then(function(data) {
                $scope.item.done = val;
              }, function(error) {
                // TODO Handle errors
                console.log('Error setting done for item ' + $scope.item.id + ' ' + error.data);
              });
          }
        }
      });
      
      $scope.removeItem = function() {
        var modalInstance = $modal.open({
          animation: true,
          templateUrl: 'DeleteDialogContent.html',
          controller: 'DeleteDialogCtrl'
        });

        modalInstance.result.then(function() {
          TodoItem.delete({itemId: $scope.item.id}, function(data) {
            var items = $scope.$parent.list.items;
            items.splice(items.indexOf($scope.item), 1);
          }, function(error) {
            // TODO Handle errors
            console.log('Error deleting item ' + $scope.item.id + ' ' + error.data);
          });
        });
      };
    }]);

  todoControllers.controller('DeleteDialogCtrl', ['$scope', '$modalInstance',
    function ($scope, $modalInstance) {
      $scope.ok = function () {
        $modalInstance.close();
      };

      $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
      };
    }]);
});