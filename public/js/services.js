/*global define */

'use strict';

define(['angular'], function(angular) {

/* Services */

// Demonstrate how to register services
// In this case it is a simple value service.
var todoServices = angular.module('todoServices', ['ngResource']);

todoServices.factory('TodoList', ['$resource',
  function($resource){
    return $resource('/api/v1/todoLists/:listId', {}, {
      query: {method:'GET', isArray:true},
      update: {method:'PUT'}
    });
  }]);

todoServices.factory('TodoItem', ['$resource',
  function($resource){
    return $resource('/api/v1/todoItems/:itemId', {}, {
      query: {method:'GET', isArray:true},
      update: {method:'PUT'}
    });
  }]);

});