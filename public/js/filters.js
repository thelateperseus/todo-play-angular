/*global define */

'use strict';

define(['angular'], function(angular) {

/* Filters */

angular.module('todoFilters', [])
  .filter('playErrorMessage', [function() {
    return function(error) {
      if (error.msg[0] === 'error.minLength') {
        return 'Please enter at least ' + error.args[0] + ' character(s)';
      }
      else {
        return JSON.stringify(error);
      }
    }
  }]);

});