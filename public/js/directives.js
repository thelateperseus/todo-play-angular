/*global define */

'use strict';

define(['angular'], function(angular) {

/* Directives */

  var todoDirectives = angular.module('todoDirectives', []);

  todoDirectives.directive('focusOnShow', function($timeout) {
    return {
      restrict: 'A',
      link: function($scope, $element, $attr) {
        if ($attr.ngShow){
          $scope.$watch($attr.ngShow, function(newValue){
            if(newValue){
              $timeout(function(){
                $element[0].focus();
              }, 0);
            }
          })      
        }
        if ($attr.ngHide){
          $scope.$watch($attr.ngHide, function(newValue){
            if(!newValue){
              $timeout(function(){
                $element[0].focus();
              }, 0);
            }
          })      
      }

      }
    };
  })

  /**
   * This directive will find itself inside HTML as a class,
   * and will remove that class, so CSS will remove loading image and show app content.
   * It is also responsible for showing/hiding login form.
   */
  todoDirectives.directive('loginDialog', function() {
    return {
      restrict: 'A',
      template: '<div ng-if="visible" ng-include="\'partials/loginForm.html\'">',
      link: function(scope, elem, attrs) {
        var showDialog = function () {
          scope.visible = true;
          elem.closest('body').find('#content').hide();
        };
        var hideDialog = function () {
          scope.visible = false;
          elem.closest('body').find('#content').show();
        };
    
        scope.visible = false;
        scope.$on('event:auth-loginRequired', showDialog);
        scope.$on('event:auth-session-timeout', showDialog)
        scope.$on('event:auth-loginConfirmed', hideDialog)
      }
    }
  });

});